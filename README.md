# htod

Hacker Dictionary Term of the Day(HTOD) where a random term is pulled and posted to Twitter once a day. 

## Running the Code

In order to run this code you will need to install the TwitterFollowBot Python package by running:

    sudo pip/pip3 install TwitterFollowBot

The shell script(htod.sh) assumes you cloned the repo to ~/git/htod. You will need to change this if you want it to run somewhere else.


### API Config File
For security reasons, the config.txt file is not included, which contains the Twitter API tokens and keys needed. You will need to create this file and enter the parms that correlate to the Twitter account that you want to post the tweets to. Syntax should be:

- OAUTH_TOKEN:
- OAUTH_SECRET:
- CONSUMER_KEY:
- CONSUMER_SECRET:
- TWITTER_HANDLE:
- ALREADY_FOLLOWED_FILE:already-followed.txt
- FOLLOWERS_FILE:followers.txt
- FOLLOWS_FILE:follows.txt
- FOLLOW_BACKOFF_MIN_SECONDS:10
- FOLLOW_BACKOFF_MAX_SECONDS:60

Consult: https://www.slickremix.com/docs/how-to-get-api-keys-and-tokens-for-twitter for how to create a Twitter application where you can generate keys & tokens.

### SQLite3 DB
You will notice in the main Python script that there is logic around adding each runs term to a SQLite3 DB. Feel free to remove this as I just added it so I could build trends to see if certain terms were repeating so as to determine the randomness of the random calls. 