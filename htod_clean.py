#!/usr/bin/python3.8

"""
Author: Justin R. Andrusk <jra@andrusk.com>
"""

def getJargonTerm():
   import random, string
   
   c = random.randint(0,2297)  # Choose a random element
   with open('/home/jra/src/scripts/htod/jargon-words.txt', 'r') as fh:
      x = fh.readlines()

      entry = x[c].rstrip()
      
      return(entry)

def tweetJargonTerm(entry):
   import re, os

   ##################################################################
   # Twitter Follow Bot Calls
   ##################################################################
   from TwitterFollowBot import TwitterBot

   my_bot = TwitterBot('/home/jra/src/scripts/config.txt')
   baseurl = 'http://andrusk.com/jargon/html/'
   letter = entry[0].upper()
      
   suburl1 = baseurl + letter + '/' + entry
   article = entry
   article_len = len(article)
   # print("Length of article is %d\n" % (article_len))
   print("Article is %s\n" % (suburl1))

   if (re.search('[A-z]\.html', article)):
      objFoulMatch = (re.search('[f][F][u][U][c][C][k][K]', article))

   if not objFoulMatch:
      hackdict = suburl1 
      term = article.split('.', 1)
      
      my_bot.sync_follows()
      print("Term is %s\n" % (term[0]))
      my_bot.send_tweet("Hacker Dictionary Term of the Day is " + term[0] + " #hackdict #opensource #floss #unix #linux #hackerdictionary #jargon " + hackdict)
      insertDB(term[0])
   else:
      pass 
   

def insertDB(htod_term):
   import sqlite3, datetime

   dt_date = datetime.datetime.now()
   date_string = dt_date.strftime('%m/%d/%Y')
   

   con = sqlite3.connect('htod.db')

   entities = (date_string, htod_term)
   cursorObj = con.cursor()
   cursorObj.execute('INSERT INTO posts(date_posted, term_posted) VALUES(?, ?)', entities)

   con.commit() 


def main():
   jargon_entry = getJargonTerm()
   tweetJargonTerm(jargon_entry)
   

if __name__ == "__main__":
	main()

